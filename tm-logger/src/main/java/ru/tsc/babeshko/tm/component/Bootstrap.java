package ru.tsc.babeshko.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.babeshko.tm.api.service.IReceiverService;
import ru.tsc.babeshko.tm.listener.LogListener;

@Component
public class Bootstrap {

    @NotNull
    @Autowired
    private IReceiverService receiverService;

    @NotNull
    @Autowired
    private LogListener logListener;

    @SneakyThrows
    public void init() {
        receiverService.init(logListener);
    }

}