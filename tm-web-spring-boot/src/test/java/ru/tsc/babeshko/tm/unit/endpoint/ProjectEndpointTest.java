package ru.tsc.babeshko.tm.unit.endpoint;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.tsc.babeshko.tm.marker.UnitCategory;
import ru.tsc.babeshko.tm.model.Project;
import ru.tsc.babeshko.tm.service.ProjectService;
import ru.tsc.babeshko.tm.service.UserService;
import ru.tsc.babeshko.tm.util.UserUtil;

import java.util.Arrays;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class ProjectEndpointTest {

    @NotNull
    private static final String PROJECT_URL = "http://localhost:8080/api/project/";

    @NotNull
    @Autowired
    private ProjectService projectService;

    @NotNull
    @Autowired
    private UserService userService;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext context;

    private final Project project = new Project("test");

    @Nullable
    private static String USER_ID;

    @Before
    public void init() {
        userService.createUser("test", "test", null);
        mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
        @NotNull final UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken("test", "test");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        USER_ID = UserUtil.getUserId();
        save(project);
    }

    @After
    public void destroy() {
        projectService.clearByUserId(USER_ID);
    }

    @Test
    public void saveTest() {
        save(new Project());
        Assert.assertEquals(2, findAll().size());
    }

    @SneakyThrows
    private void save(@NotNull final Project project) {
        @NotNull String url = PROJECT_URL + "save";
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(project);
        mockMvc.perform(
                MockMvcRequestBuilders.post(url)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().isOk());
    }

    @Test
    public void findAllTest() {
        Assert.assertEquals(1, findAll().size());
        save(new Project());
        Assert.assertEquals(2, findAll().size());
    }

    @NotNull
    @SneakyThrows
    private List<Project> findAll() {
        @NotNull final String url = PROJECT_URL + "findAll";
        @NotNull final String json = mockMvc.perform(
                MockMvcRequestBuilders.get(url)
                        .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        return Arrays.asList(objectMapper.readValue(json, Project[].class));
    }

    @Test
    public void findByIdTest() {
        @Nullable final Project projectFromDB = findById(project.getId());
        Assert.assertNotNull(projectFromDB);
        Assert.assertEquals(project.getId(), projectFromDB.getId());
    }

    @Nullable
    @SneakyThrows
    private Project findById(@NotNull final String id) {
        @NotNull String url = PROJECT_URL + "findById/" + id;
        @NotNull final String json =
                mockMvc.perform(
                        MockMvcRequestBuilders.get(url).contentType(MediaType.APPLICATION_JSON)
                ).andDo(print()).andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
        if (json.isEmpty()) return null;
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(json, Project.class);
    }

    @Test
    public void existsByIdTest() {
        Assert.assertEquals(true, existsById(project.getId()));
    }

    @SneakyThrows
    public Boolean existsById(@NotNull final String id) {
        @NotNull String url = PROJECT_URL + "existsById/" + id;
        @NotNull final String json =
                mockMvc.perform(
                        MockMvcRequestBuilders.get(url).contentType(MediaType.APPLICATION_JSON)
                ).andDo(print()).andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
        if (json.isEmpty()) return false;
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(json, Boolean.class);
    }

    @Test
    public void deleteTest() {
        delete(project);
        Assert.assertEquals(0, findAll().size());
    }

    @SneakyThrows
    public void delete(@NotNull final Project project) {
        @NotNull final String url = PROJECT_URL + "delete";
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writeValueAsString(project);
        mockMvc.perform(
                MockMvcRequestBuilders.post(url).content(json).contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().isOk());
    }

    @Test
    public void deleteByIdTest() {
        Assert.assertEquals(1, findAll().size());
        deleteById(project.getId());
        Assert.assertNull(findById(project.getId()));
        Assert.assertEquals(0, findAll().size());
    }

    @SneakyThrows
    public void deleteById(@NotNull final String id) {
        @NotNull final String url = PROJECT_URL + "deleteById/" + id;
        mockMvc.perform(
                MockMvcRequestBuilders.delete(url).contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().isOk());
    }

    @Test
    public void clearTest() {
        Assert.assertEquals(1, findAll().size());
        clear();
        Assert.assertEquals(0, findAll().size());
    }

    @SneakyThrows
    private void clear() {
        mockMvc.perform(
                MockMvcRequestBuilders.delete(PROJECT_URL + "clear")
                        .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().isOk());
    }

    @Test
    public void countTest() {
        Assert.assertEquals(1, count());
    }

    @SneakyThrows
    public long count() {
        @NotNull final String countUrl = PROJECT_URL + "count";
        @NotNull final String json = mockMvc.perform(
                MockMvcRequestBuilders.get(countUrl).contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(json, Long.class);
    }

}