package ru.tsc.babeshko.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.tsc.babeshko.tm.api.TaskEndpoint;
import ru.tsc.babeshko.tm.model.Task;
import ru.tsc.babeshko.tm.service.TaskService;
import ru.tsc.babeshko.tm.util.UserUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/task")
@WebService(endpointInterface = "ru.tsc.babeshko.tm.api.TaskEndpoint")
public class TaskEndpointImpl implements TaskEndpoint {

    @NotNull
    @Autowired
    private TaskService taskService;

    @Override
    @WebMethod
    @GetMapping("/findAll")
    public List<Task> findAll() {
        return new ArrayList<>(taskService.findAllByUserId(UserUtil.getUserId()));
    }

    @Override
    @WebMethod
    @GetMapping("/findById/{id}")
    public Task findById(
            @NotNull
            @WebParam(name = "id")
            @PathVariable("id") final String id
    ) {
        return taskService.findByIdAndUserId(id, UserUtil.getUserId());
    }

    @Override
    @WebMethod
    @GetMapping("/existsById/{id}")
    public boolean existsById(
            @NotNull
            @WebParam(name = "id")
            @PathVariable("id") final String id
    ) {
        return taskService.existsByIdAndUserId(id, UserUtil.getUserId());
    }

    @Override
    @WebMethod
    @PostMapping("/save")
    public Task save(
            @NotNull
            @WebParam(name = "task")
            @RequestBody final Task task
    ) {
        return taskService.save(task, UserUtil.getUserId());
    }

    @Override
    @WebMethod
    @PostMapping("/delete")
    public void delete(
            @NotNull
            @WebParam(name = "task")
            @RequestBody final Task task
    ) {
        taskService.remove(task, UserUtil.getUserId());
    }

    @Override
    @WebMethod
    @PostMapping("/deleteAll")
    public void deleteAll(
            @NotNull
            @WebParam(name = "tasks")
            @RequestBody final List<Task> tasks
    ) {
        taskService.remove(tasks, UserUtil.getUserId());
    }

    @Override
    @WebMethod
    @DeleteMapping("/clear")
    public void clear() {
        taskService.clearByUserId(UserUtil.getUserId());
    }

    @Override
    @WebMethod
    @DeleteMapping("/deleteById/{id}")
    public void deleteById(
            @NotNull
            @WebParam(name = "id")
            @PathVariable("id") final String id
    ) {
        taskService.removeByIdAndUserId(id, UserUtil.getUserId());
    }

    @Override
    @WebMethod
    @GetMapping("/count")
    public long count() {
        return taskService.countByUserId(UserUtil.getUserId());
    }

}