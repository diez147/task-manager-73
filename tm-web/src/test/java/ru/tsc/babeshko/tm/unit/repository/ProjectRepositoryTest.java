package ru.tsc.babeshko.tm.unit.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.babeshko.tm.configuration.ApplicationConfiguration;
import ru.tsc.babeshko.tm.marker.UnitCategory;
import ru.tsc.babeshko.tm.model.Project;
import ru.tsc.babeshko.tm.repository.ProjectRepository;

import java.util.List;
import java.util.UUID;

@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApplicationConfiguration.class})
public class ProjectRepositoryTest {

    @NotNull
    private static final String USER_ID = UUID.randomUUID().toString();

    @NotNull
    @Autowired
    private ProjectRepository repository;

    @NotNull
    private final Project project = new Project("test");

    @Before
    public void init() {
        project.setUserId(USER_ID);
        repository.save(project);
    }

    @After
    @Transactional
    public void destroy() {
        repository.delete(project);
    }

    @Test
    public void findByIdAndUserId() {
        @Nullable Project projectFromDB = repository.findByIdAndUserId(project.getId(), USER_ID).orElse(null);
        Assert.assertNotNull(projectFromDB);
        Assert.assertEquals(project.getName(), projectFromDB.getName());
    }

    @Test
    public void findAllByUserId() {
        List<Project> projects = repository.findAllByUserId(USER_ID);
        Assert.assertNotNull(projects);
        Assert.assertEquals(1, projects.size());
    }

    @Test
    public void countByUserId() {
        long count = repository.countByUserId(USER_ID);
        Assert.assertEquals(1, count);
    }

    @Test
    public void existsByIdAndUserId() {
        boolean exists = repository.existsByIdAndUserId(project.getId(), USER_ID);
        Assert.assertTrue(exists);
    }

    @Test
    @Transactional
    public void deleteByIdAndUserId() {
        repository.deleteByIdAndUserId(project.getId(), USER_ID);
        Assert.assertEquals(0, repository.countByUserId(USER_ID));
    }

    @Test
    @Transactional
    public void deleteAllByUserId() {
        repository.deleteAllByUserId(USER_ID);
        Assert.assertEquals(0, repository.countByUserId(USER_ID));
    }

}