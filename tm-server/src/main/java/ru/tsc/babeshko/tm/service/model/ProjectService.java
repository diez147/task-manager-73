package ru.tsc.babeshko.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tsc.babeshko.tm.api.service.model.IProjectService;
import ru.tsc.babeshko.tm.exception.entity.UserNotFoundException;
import ru.tsc.babeshko.tm.exception.field.EmptyDescriptionException;
import ru.tsc.babeshko.tm.exception.field.EmptyNameException;
import ru.tsc.babeshko.tm.exception.field.EmptyUserIdException;
import ru.tsc.babeshko.tm.model.Project;
import ru.tsc.babeshko.tm.model.User;
import ru.tsc.babeshko.tm.repository.model.ProjectRepository;

import java.util.Date;

@Service
public class ProjectService extends AbstractUserOwnedService<Project> implements IProjectService {

    @NotNull
    @Autowired
    private ProjectRepository repository;

    @NotNull
    @Override
    protected ProjectRepository getRepository() {
        return repository;
    }

    @Nullable
    @Override
    public Project create(@Nullable final User user, @Nullable final String name) {
        if (user == null) throw new UserNotFoundException();
        if (user.getId().isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable Project project = new Project();
        project.setName(name);
        project.setUser(user);
        repository.save(project);
        return project;
    }

    @Nullable
    @Override
    public Project create(
            @Nullable final User user,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (user == null) throw new UserNotFoundException();
        if (user.getId().isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @Nullable Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        project.setUser(user);
        repository.save(project);
        return project;
    }

    @Nullable
    @Override
    public Project create(
            @Nullable final User user,
            @Nullable final String name,
            @Nullable final String description,
            @Nullable final Date dateBegin,
            @Nullable final Date dateEnd
    ) {
        if (user == null) throw new UserNotFoundException();
        if (user.getId().isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @Nullable Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        project.setUser(user);
        project.setDateBegin(dateBegin);
        project.setDateEnd(dateEnd);
        repository.save(project);
        return project;
    }

    @Override
    public void removeAllByUserId(final @Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        repository.deleteAllByUserId(userId);
    }

}