package ru.tsc.babeshko.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tsc.babeshko.tm.api.service.model.ITaskService;
import ru.tsc.babeshko.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.babeshko.tm.exception.entity.UserNotFoundException;
import ru.tsc.babeshko.tm.exception.field.EmptyDescriptionException;
import ru.tsc.babeshko.tm.exception.field.EmptyNameException;
import ru.tsc.babeshko.tm.exception.field.EmptyUserIdException;
import ru.tsc.babeshko.tm.model.Task;
import ru.tsc.babeshko.tm.model.User;
import ru.tsc.babeshko.tm.repository.model.TaskRepository;

import java.util.Date;
import java.util.List;

@Service
public class TaskService extends AbstractUserOwnedService<Task> implements ITaskService {

    @NotNull
    @Autowired
    private TaskRepository repository;

    @NotNull
    @Override
    protected TaskRepository getRepository() {
        return repository;
    }

    @Nullable
    @Override
    public Task create(@Nullable final User user, @Nullable final String name) {
        if (user == null) throw new UserNotFoundException();
        if (user.getId().isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable Task task = new Task();
        task.setName(name);
        task.setUser(user);
        repository.save(task);
        return task;
    }

    @Nullable
    @Override
    public Task create(
            @Nullable final User user,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (user == null) throw new UserNotFoundException();
        if (user.getId().isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @Nullable Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        task.setUser(user);
        repository.save(task);
        return task;
    }

    @Nullable
    @Override
    public Task create(
            @Nullable final User user,
            @Nullable final String name,
            @Nullable final String description,
            @Nullable final Date dateBegin,
            @Nullable final Date dateEnd
    ) {
        if (user == null) throw new UserNotFoundException();
        if (user.getId().isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @Nullable Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        task.setUser(user);
        task.setDateBegin(dateBegin);
        task.setDateEnd(dateEnd);
        repository.save(task);
        return task;
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        return repository.findAllByProjectIdAndUserId(projectId, userId);
    }

    @Override
    public void removeAllByProjectId(final @Nullable String userId, final @Nullable String projectId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        repository.deleteAllByProjectIdAndUserId(projectId, userId);
    }

    @Override
    public void removeAllByUserId(final @Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        repository.deleteAllByUserId(userId);
    }

}